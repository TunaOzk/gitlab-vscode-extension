import vscode from 'vscode';
import { tokenService } from '../services/token_service';

export async function installRedhatYamlExtension() {
  await vscode.commands.executeCommand(
    'workbench.extensions.installExtension',
    'redhat.vscode-yaml',
  );
}

export async function suggestYamlExtensionIfNotInstalled(): Promise<void> {
  const documentName = vscode.window.activeTextEditor?.document;
  if (documentName === undefined) return;
  const filename: string = documentName.fileName;
  const regex = /^.*\.gitlab-ci\.(yml)|(yaml)$/;
  if (!regex.test(filename)) return;
  const gitExtension = vscode.extensions.getExtension('redhat.vscode-yaml');
  if (gitExtension) return;
  if (tokenService.context?.globalState.get('decisionForInstallingYamlExtension') === 'dontAsk')
    return;
  const message = `YAML Extension provides comprehensive YAML Language support to Visual Studio Code,
      the yaml-language-server, with built-in Kubernetes syntax support. Would you like the install it? `;
  const options = ['Yes', 'Not Now', "No.Don't ask again."];
  const selection = await vscode.window.showInformationMessage(message, ...options);
  switch (selection) {
    case 'Yes':
      await installRedhatYamlExtension();
      break;
    case 'Not Now':
      break;
    case "No.Don't ask again.": {
      await tokenService.context?.globalState.update(
        'decisionForInstallingYamlExtension',
        'dontAsk',
      );
      break;
    }
    default:
      break;
  }
}
